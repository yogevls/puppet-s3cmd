class s3cmd::s3cmd-cron-task (
	$user = hiera('user_cron'),
	$task_name = hiera('s3_task_name'),
	$command = hiera('s3_cron_command'))
	
	{	

cron { $task_name:
	command  => $command,
	user     => $user,
	month    => "*",
	monthday => "*",
	hour     => "*",
	minute   => "*"
	}
}
