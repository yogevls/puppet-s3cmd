class s3cmd::s3cmd (
	$user = hiera('user'),
	$aws_access_key_id= hiera('aws_access_key_id'),
	$aws_secret_access_key= hiera('aws_secret_access_key'))

{

package { "s3cmd":
	ensure => present,
	}


file { "s3cfg":
    path => "/home/$user/.s3cfg",
    content => template("s3cmd/s3cfg.erb"),
    ensure => present
	}
  
}
