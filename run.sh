#!/bin/bash -x
puppet apply --modulepath ./modules $@ --confdir ./conf  --hiera_config ./hieradata/hiera.yaml --verbose --debug
rm -rf log/ run/ ssl/ var/ /conf/ssl